document.querySelectorAll(['c', 'ic']).forEach((element) => {
    // Function to replace text with span using color
    const wrapWithSpan = (text, color) => `<span style="color: ${color};">${text}</span>`;

    // Keywords
    const primaryKeywords = /\b(def|class|return|pass|break|continue)\b/g;
    const controlFlowKeywords = /\b(if|else|elif|for|while|try|except|finally|with|as|yield|in)\b/g;
    const importKeywords = /\b(import|from)\b/g;
    const functionKeywords = /\b(lambda|self|range|min|max)\b/g;

    // Strings and comments - we will now use a callback to correctly handle the replacement
    const strings = /(['"])(?:(?=(\\?))\2.)*?\1/g;
    const comments = /# [^<]*(?=<br>)/g


    let htmlContent = element.innerHTML;

    // Replace strings and comments first to avoid parsing issues with other replacements
    htmlContent = htmlContent.replace(strings, match => wrapWithSpan(match, positive));
    htmlContent = htmlContent.replace(comments, match => wrapWithSpan(match, '#1292BD'));

    // Replace keywords, matching colors from the palette
    htmlContent = htmlContent.replace(primaryKeywords, match => wrapWithSpan(match, '#8C6DC5'));
    htmlContent = htmlContent.replace(controlFlowKeywords, match => wrapWithSpan(match, '#0DBCB4'));
    htmlContent = htmlContent.replace(importKeywords, match => wrapWithSpan(match, '#87D687'));
    htmlContent = htmlContent.replace(functionKeywords, match => wrapWithSpan(match, '#D35E91'));

    // Preserve <br> and <span> by using placeholders
    const brPlaceholder = "PLACEHOLDER_FOR_BR";
    const spanPlaceholderOpen = "PLACEHOLDER_FOR_SPAN_OPEN";
    const spanPlaceholderClose = "PLACEHOLDER_FOR_SPAN_CLOSE";

    htmlContent = htmlContent.replace(/<br>/g, brPlaceholder);
    htmlContent = htmlContent.replace(/<span/g, spanPlaceholderOpen);
    htmlContent = htmlContent.replace(/<\/span>/g, spanPlaceholderClose);

    // Revert placeholders back to original tags
    htmlContent = htmlContent.replace(new RegExp(brPlaceholder, 'g'), '<br>');
    htmlContent = htmlContent.replace(new RegExp(spanPlaceholderOpen, 'g'), '<span');
    htmlContent = htmlContent.replace(new RegExp(spanPlaceholderClose, 'g'), '</span>');

    // Set the innerHTML with the replacements
    element.innerHTML = htmlContent;
});