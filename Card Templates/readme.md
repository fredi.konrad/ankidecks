# Card Templates

This folder contains the source code of the Librico Card Templates. There is currently no way to edit them globally within Anki, so this is the best I can do. If you feel like your cards are missing a feature, paste the code you find in the folder `aggregated` into your local template. 
